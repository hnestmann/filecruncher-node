

var _ = require('underscore');

var unflatten = function( array, parent, tree ){

    tree = typeof tree !== 'undefined' ? tree : [];
    parent = typeof parent !== 'undefined' ? parent : { id: 'root' };

    var children = _.filter( array, function(child){
    	return child.parent == parent.id;
    });

    if( !_.isEmpty( children )  ){
        if( parent.id === 'root' ){
           tree = children;
        }else{
           parent.children = children;
        }
        _.each( children, function( child ){ unflatten( array, child ); } );
    }
    return tree;
};

var path = process.cwd() + '/' + process.argv[2];

var fs        = require('fs');

var XmlStream = require('xml-stream');

var stream=fs.createReadStream(path);
var xml = new XmlStream(stream);

xml.preserve('category', true);
var categoryTree = {};
var categoryElements = [];
var currentCategory;

xml.on('startElement: category', function(item) {
  currentCategory = new Object();
  currentCategory.id = item.$['category-id']
});

xml.on('text: category > display-name', function(item) {
  if (!currentCategory.displayName) {
   	currentCategory.displayName = new Object();
  }
  var lang = item.$['xml:lang'];
 if (lang === 'x-default') {
	currentCategory.defaultName = item.$text;
 } else {
 	if (lang.indexOf('-') > -1) {
 		currentCategory.displayName[lang] = item.$text;
 	}
 }

});

xml.on('text: category > parent', function(item) {
  currentCategory.parent = item.$text;
});

xml.on('text: category > online-flag', function(item) {
  currentCategory.online = item.$text;
});

xml.on('endElement: category', function(item) {
 if (currentCategory.id !== 'root' && currentCategory.online === 'true') {
 	categoryElements.push(currentCategory);
 }
});

xml.on('end', function(item) {
	var tree = unflatten(categoryElements);

	fs.writeFile(process.cwd() + '/tree.json', JSON.stringify(tree, null, 4), function(err) {
	    if(err) {
	        return console.log(err);
	    }

	    console.log("The file was saved!");
	});
});

