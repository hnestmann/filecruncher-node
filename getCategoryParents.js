var path = process.cwd() + '/' + process.argv[2];

var fs = require('fs');

var XmlStream = require('xml-stream');

var stream = fs.createReadStream(path);
var xml = new XmlStream(stream);

xml.preserve('category', true);
var categoryElements = {};
var currentCategory;

xml.on('startElement: category', function(item) {
    if (currentCategory) {
        categoryElements[currentCategory.id] = currentCategory;
    }

    currentCategory = new Object();
    currentCategory.id = item.$['category-id'];
});

xml.on('text: category > parent', function(item) {
    currentCategory.parent = item.$text;
});

xml.on('end', function(item) {
    categoryElements[currentCategory.id] = currentCategory;

    fs.writeFile(process.cwd() + '/category-imports/category-mapping-in/parents.json', JSON.stringify(categoryElements, null, 4), function(err) {
      if (err) {
          return console.log(err);
      }

      console.log('The file was saved!');
  });
});

