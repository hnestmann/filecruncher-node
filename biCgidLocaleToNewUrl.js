var fs = require('fs');
var fileContent = fs.readFileSync(process.cwd() + '/' + process.argv[2], 'utf8');
var lines = fileContent.split('\n');
var currentCategoryRegExp = /<!-- CategoryID: (.*) Locale: (.*) -->/;
var currentURLRegexp = new RegExp('<url><loc>http:\/\/www.bianco.com\/[a-z]{2}\/[a-z]{2}(.*)<\/loc>');
var currentCategory;
var currentLocale;
var mappings = [];

for (var i = 0; i < lines.length; i++) {
    var currentLine = lines[i];
    if (currentLine.indexOf('<!-- CategoryID: ') !== -1) {
        currentCategory = currentCategoryRegExp.exec(currentLine)[1];
        currentLocale = currentCategoryRegExp.exec(currentLine)[2];
    }

    if (currentLine.indexOf('<url><loc>') !== -1) {
        var currentUrl = currentURLRegexp.exec(currentLine)[1];
        mappings.push({ currentCategory, currentLocale, currentUrl });
    }
}

fs.appendFileSync(process.cwd() + '/urls/' + 'cgidIdsToNewPath.json', JSON.stringify(mappings));
