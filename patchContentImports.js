var fs = require('fs');
var parseString = require('xml2js').parseString;
var namesPath = process.cwd() + '/' + process.argv[2];
var names = JSON.parse(fs.readFileSync(namesPath, 'utf8'));
var baseImports = process.cwd() + '/' + process.argv[3];

var files = fs.readdirSync(baseImports);

for (var index in files) {
    var file = baseImports + '/' + files[index];

    if (file.indexOf('.xml') !== -1) {
        var lineReader = require('readline').createInterface({
            input: require('fs').createReadStream(file)
        });
        var currentContentID = '';
        lineReader.on('line', function(line) {
            var assetIDmatches = line.match(/content-id=\"(.*?)\"/);
            if (assetIDmatches && assetIDmatches.length === 2) {
                currentContentID = assetIDmatches[1];
            }

            if (line.indexOf('</custom-attributes>') !== -1) {
                var categoryName = names[currentContentID.replace('-top-banner', '')];
                if (categoryName) {
                    for (var locale in categoryName.displayName) {
                        var headerString = '<custom-attribute attribute-id="header" xml:lang="' + locale + '"><![CDATA[' + categoryName.displayName[locale] + ']]></custom-attribute>';
                        fs.appendFileSync(this.input.path + '.patched.xml', headerString + '\n');
                    }
                }
            }

            fs.appendFileSync(this.input.path + '.patched.xml', line.toString() + '\n');
        });
    }
}
