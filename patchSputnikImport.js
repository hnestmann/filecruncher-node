var fs = require('fs');
var mappingPath = process.cwd() + '/' + process.argv[2];
var bingAndParentsPath = process.cwd() + '/' + process.argv[3];
var sputnikMap = JSON.parse(fs.readFileSync(mappingPath, 'utf8'));
var bingAndParentsMap = JSON.parse(fs.readFileSync(bingAndParentsPath, 'utf8'));
var file = process.cwd() + '/' + process.argv[4];
var skipNonExistingCategory = false;
var brandsToPatch = ['jj-', 'sl-', 'on-', 'os-', 'mm-', 'jr-', 'ni-'];

if (file.indexOf('.xml') !== -1) {
    var lineReader = require('readline').createInterface({
            input: require('fs').createReadStream(file)
        });
    var currentCategoryID = '';
    lineReader.on('line', function(line) {
        var categoryIDmatches = line.match(/category-id=\"(.*?)\"/);
        if (categoryIDmatches && categoryIDmatches.length === 2) {
            currentCategoryID = categoryIDmatches[1];

            skipNonExistingCategory = false;
            if (brandsToPatch.indexOf(currentCategoryID.substring(0, 2) + '-') !== -1) {
                skipNonExistingCategory = true;
            } else {
                if (sputnikMap[currentCategoryID]) {
                    currentCategoryID = sputnikMap[currentCategoryID];
                }

                if (bingAndParentsMap[currentCategoryID]) {
                    var linetoPrint = '<category category-id="' + bingAndParentsMap[currentCategoryID].id + '">';
                    if (bingAndParentsMap[currentCategoryID].parent) {
                        linetoPrint += '<parent>' + bingAndParentsMap[currentCategoryID].parent + '</parent>';
                    }

                    fs.appendFileSync(process.cwd() + '/category-imports/bing-out/' + 'out.xml', linetoPrint + '\n');
                } else {
                    skipNonExistingCategory = true;
                }
            }
        } else {
            if (line.indexOf('<parent') === -1 && line.indexOf('online-from') === -1 && !skipNonExistingCategory) {
                fs.appendFileSync(process.cwd() + '/category-imports/bing-out/' + 'out.xml', line.toString() + '\n');
            }
        }
    });
}
