var fs = require('fs');
var file = process.cwd() + '/' + process.argv[2];
var counterObject = {};
var internalPosition = 0;
var countPositions = false;
if (file.indexOf('.xml') !== -1) {
    var lineReader = require('readline').createInterface({
            input: fs.createReadStream(file)
        });
    lineReader.on('line', function(line) {
        if (line.indexOf('<image-group view-type="large"') > 0) {
            internalPosition = 0;
            countPositions = true;
        }

        if (countPositions) {
            var positionParser = new RegExp('<image path="large/.*?_([0-9]{3})_ProductLarge\.jpg"/>');
            var matches = line.match(positionParser);
            if (matches) {
                if (!counterObject[matches[1]]) {
                    counterObject[matches[1]] = {};
                    counterObject[matches[1]].total = 0;
                    counterObject[matches[1]].inPosition = 0;
                }

                counterObject[matches[1]].total++;

                if ((parseInt(matches[1]) - 1) === internalPosition) {
                    counterObject[matches[1]].inPosition++;
                }

                internalPosition++;
            }
        }

        if (line.indexOf('</image-group>') > 0) {
            countPositions = false;
        }

        //console.info(JSON.stringify(counterObject));
    });

    lineReader.on('close', function() {
        console.info(JSON.stringify(counterObject));
    });
}
