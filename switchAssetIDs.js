var fs = require('fs');
var mappingPath = process.cwd() + '/' + process.argv[2];
var mapping = JSON.parse(fs.readFileSync(mappingPath, 'utf8'));
var baseImports = process.cwd() + '/' + process.argv[3];

var files = fs.readdirSync(baseImports);

for (var index in files) {
    var file = baseImports + '/' + files[index];

    if (file.indexOf('.xml') !== -1) {
        var lineReader = require('readline').createInterface({
            input: require('fs').createReadStream(file)
        });
        var currentContentID = '';
        lineReader.on('line', function(line) {
            var assetIDmatches = line.match(/content-id=\"(.*?)-top-banner\"/);
            if (assetIDmatches && assetIDmatches.length === 2) {
                currentContentID = assetIDmatches[1];
                var newLine = '<content content-id="' + mapping[currentContentID] + '">';
                fs.appendFileSync(this.input.path + '.patched.xml', newLine + '\n');
            } else {
                fs.appendFileSync(this.input.path + '.patched.xml', line.toString() + '\n');
            }
        });
    }
}
