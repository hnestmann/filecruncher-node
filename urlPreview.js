var traverse = require('traverse');
var _ = require('underscore');

var fs  = require('fs');

var path = process.cwd() + '/' + process.argv[2];

var tree = JSON.parse(fs.readFileSync(path, "utf8"));
var languages = [];
var newTree = {};
String.prototype.replaceAll = function( token, newToken, ignoreCase ) {
    var _token;
    var str = this + "";
    var i = -1;

    if ( typeof token === "string" ) {

        if ( ignoreCase ) {

            _token = token.toLowerCase();

            while( (
                i = str.toLowerCase().indexOf(
                    _token, i >= 0 ? i + newToken.length : 0
                ) ) !== -1
            ) {
                str = str.substring( 0, i ) +
                    newToken +
                    str.substring( i + token.length );
            }

        } else {
            return this.split( token ).join( newToken );
        }

    }
return str;
};

// getting languages
traverse(tree).forEach(function (x) {
    if (typeof(this.node) === 'object' && 'displayName' in this.node) {
      for (var language in this.node.displayName) {
        languages.push(language);
        languages = _.uniq(languages);
      }
    }
});

traverse(tree).forEach(function (x) {
    if (typeof(this.node) === 'object' && 'id' in this.node) {
      var urls = [];
      var id = this.node.id;
      var _this = this;
      languages.forEach(function(language) {
        var path = '';
        var currentNode = _this;
        while (currentNode.parent) {
          if (typeof(currentNode.node) === 'object' && 'displayName' in currentNode.node) {
            var pathElement = '';
            if (language in currentNode.node.displayName && typeof (currentNode.node.displayName[language]) !== 'undefined') {
              pathElement = currentNode.node.displayName[language] ;
            } else {
              if (typeof (currentNode.node.defaultName) !== 'undefined') {
                pathElement = currentNode.node.defaultName;
              }
            }

            pathElement = pathElement.replaceAll(' ', '-');
            pathElement = pathElement.replaceAll('Ü','ue');
            pathElement = pathElement.replaceAll('ß','ss');
            pathElement = pathElement.replaceAll('&amp;','and');
            pathElement = pathElement.replaceAll('Ö','oe');
            pathElement = pathElement.replaceAll('Ø','oe');
            pathElement = pathElement.replaceAll('ø','oe');
            pathElement = pathElement.replaceAll('ä','ae');
            pathElement = pathElement.replaceAll('Ä','ae');
            pathElement = pathElement.replaceAll('Å','a');
            pathElement = pathElement.replaceAll('æ','ae');
            pathElement = pathElement.replaceAll('Æ','ae');
            pathElement = pathElement.replaceAll('ü','ue');

            path = pathElement.toLowerCase() + '/' + path;
          }


          currentNode = currentNode.parent;
        }



        var urlLanguage = language.split('-')[1].toLowerCase() + '/' + language.split('-')[0];
        path = urlLanguage + '/' + path;

        if (newTree[path]) {
          newTree[path].push(id);
        } else {
          newTree[path] = [id];
        }

      });
    }
});

var sortedTree = {};
for (var url in newTree) {
  var categoryCount = Object.keys(newTree[url]).length;
  var temporaryTreeElement = new Object();
  temporaryTreeElement[url] = newTree[url];
  if (categoryCount > 1) {
    if (sortedTree[categoryCount]) {
      sortedTree[categoryCount].push(temporaryTreeElement);
    } else {
      sortedTree[categoryCount] = [temporaryTreeElement];
    }
  }
}

var keys = Object.keys(sortedTree);
var keyLength = keys.length;

keys.sort();

newTree = new Object();

for (i = keyLength-1; i > -1; i--) {
  newTree[i] = sortedTree[i];
}

fs.writeFile(process.cwd() + '/newTree.json', JSON.stringify(newTree, null, 4), function(err) {
      if(err) {
          return console.log(err);
      }

      console.log("The file was saved!");
  });
