var fs = require('fs');
var oldMapping = eval(fs.readFileSync(process.cwd() + '/' + process.argv[2], 'utf8'));
var newMapping = JSON.parse(fs.readFileSync(process.cwd() + '/' + process.argv[3], 'utf8'));

//oldMapping = oldMapping.reverse();

/*
<?xml version="1.0" encoding="UTF-8"?>
<redirect-urls xmlns="http://www.demandware.com/xml/impex/redirecturl/2011-09-01">
    <redirect-url uri="/naiselle/kengät">
        <status-code>301</status-code>
        <enabled-flag>true</enabled-flag>
        <destination-url>/search?cgid=bt-shirts</destination-url>
        <copy-source-params>default</copy-source-params>
    </redirect-url>

</redirect-urls>
*/

fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<?xml version="1.0" encoding="UTF-8"?>');
fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<redirect-urls xmlns="http://www.demandware.com/xml/impex/redirecturl/2011-09-01">');

oldMapping.forEach(function(element, index) {
    var cgid = Object.keys(element)[0];
    var url = element[cgid];
    var newPath = '';

    newMapping.forEach(function(newElement) {
        if (newElement.currentCategory === cgid && url.indexOf('.de') !== -1 && newElement.currentLocale === 'de_DE') {
            newPath = newElement.currentUrl;
        }

        if (newElement.currentCategory === cgid && url.indexOf('.dk') !== -1 && newElement.currentLocale === 'da_DK') {
            newPath = newElement.currentUrl;
        }

        if (newElement.currentCategory === cgid && url.indexOf('.fi') !== -1 && newElement.currentLocale === 'fi_FI') {
            newPath = newElement.currentUrl;
        }

        if (newElement.currentCategory === cgid && url.indexOf('.se') !== -1 && newElement.currentLocale === 'sv_SE') {
            newPath = newElement.currentUrl;
        }

        if (newElement.currentCategory === cgid && url.indexOf('.no') !== -1 && newElement.currentLocale === 'no_NO') {
            newPath = newElement.currentUrl;
        }
    });

    var path = (new RegExp('www\.bianco\..*?/(.*)')).exec(url)[1];
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<redirect-url uri="/' + path + '*">');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<status-code>301</status-code>');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<enabled-flag>true</enabled-flag>');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<priority>' + index + 1 + '</priority>');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<destination-url>' + newPath + '</destination-url>');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '<copy-source-params>yes</copy-source-params>');
    fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '</redirect-url>');
});

fs.appendFileSync(process.cwd() + '/urls/' + 'redirect-urls.xml', '</redirect-urls>');

